/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
       import java.io.*;
import java.util.ArrayList;
import java.util.List;
/**
 *
 * @author zda
 */
public class compiler
{
    public static void main(String[] args)
    throws IOException, InterruptedException
    {
        tmp tm = new tmp();
        String output;
        //output = tm.RunWithoutCompile("/home/zda/HIDE/HW.hs");
        //List<String> filesToDelete = new ArrayList <String>();
        //output = tm.Clean("/home/zda/HIDE/","/home/zda/HIDE/");
        output = tm.makeBuild("/home/zda/HIDE/", "Makefile");
        //output = output + tm.Run("/home/zda/HIDE/HW");
        System.out.print(output);
    }  
    
}

class tmp
{
    private String ghc;
    private String runhaskell;
    tmp()
    {
        ghc = "ghc";
        runhaskell = "runhaskell";
    }
    tmp(String ghcPath, String runhaskellPath)
    {
        ghc = ghcPath;
        runhaskell = runhaskellPath;
    }
    public String RunWithoutCompile(String fullName) throws InterruptedException, IOException
    {
        String output = "";
        List <String> runCommand = new ArrayList<String>();
        runCommand.add(runhaskell);
        runCommand.add(fullName);
        
        output = executeCommand(runCommand);
        
        return output;
    }
    
    public String CompileFile(String fileFullName, List<String> compilerOptions) throws InterruptedException, IOException
    {
        String output = "";

        List<String> compile = new ArrayList<String>();
        compile.add(ghc);
        for(int j = 0; j < compilerOptions.size(); j++)
        {
            compile.add(compilerOptions.get(j));
        }
        compile.add(fileFullName);

        output = executeCommand(compile);

        return output;
    }
    
    public String Run(String binFullName) throws InterruptedException, IOException
    {
        String output = "";
        List <String> Execute = new ArrayList<String>();
        Execute.add(binFullName);
        output = Run(Execute);
        
        return output;
    }
    
    public String Run(List<String> binFullNameWithArguments) throws InterruptedException, IOException
    {
        String output = "";
        List <String> makeExecutable = new ArrayList<String>();
        makeExecutable.add("chmod");
        makeExecutable.add("a+x");
        makeExecutable.add(binFullNameWithArguments.get(0));
        output = executeCommand(makeExecutable);
        output = output + executeCommand(binFullNameWithArguments);
                
        return output;
    }
    
    // delete all files listed in filesToDeleteFullName
    public String Clean(String BinDirectory, String ObjDirectory) throws InterruptedException, IOException
    {
        String output = "";

        List <String> rmFile = new ArrayList<String>();
        rmFile.add("rm");
        rmFile.add("-v");       // add verbose output
        rmFile.add(BinDirectory + " *.o"); //[TBD] what if it has other type
        rmFile.add(ObjDirectory + " *.hi"); //[TBD] what if it has other type        
        
        output = executeCommand(rmFile);

        return output;
    }

    public String CustomCommand(String projectPath, String command) throws IOException, InterruptedException
    {
        String output = "";

        List <String> toProjectFolder = new ArrayList<String>();
        toProjectFolder.add("cd");
        toProjectFolder.add("projectPath");
        
        output = executeCommand(toProjectFolder);
        output = executeCommand(command);

        return output;
    }
    public String makeBuild(String projectPath, String Makefile) throws IOException, InterruptedException
    {
        String output;
        List <String> cmd = new ArrayList <String>();
        cmd.add("make");
        cmd.add("--directory=" + projectPath);
        cmd.add("--makefile=" + Makefile);
        cmd.add("build");       // [TBD] some reasonable value
        output = executeCommand(cmd);
        return output;
    }
    
    private String executeCommand(List <String> command) throws IOException, InterruptedException
    {
        String output = "";
          
        ProcessBuilder procBuilder = new ProcessBuilder(command); 
        procBuilder.redirectErrorStream(true);

        Process proc = procBuilder.start();

        // Redirect output stream to brStdout
        InputStream processOutput = proc.getInputStream();
        InputStreamReader outputReader = new InputStreamReader(processOutput);
        BufferedReader bufferReader = new BufferedReader(outputReader);

        // Waiting for process finished.
        proc.waitFor();
        // Fill output variable with process output
        String tmp = null;
        StringBuilder bldr = new StringBuilder(output);
        while((tmp = bufferReader.readLine()) != null)
        {
            bldr.append(tmp);
            bldr.append("\n");
        }
        output = bldr.toString();

        return output;
    }
    private String executeCommand(String command) throws IOException, InterruptedException
    {
        String output = "";          
        List <String> cmd = new ArrayList <String>();
        cmd.add(command);
        output = executeCommand(cmd);
        return output;
    }
    
}

class Project
{
    List<String> sources;
    List<String> compilerOptions;
    Project()   {
        sources = new ArrayList<String>();
        compilerOptions = new ArrayList<String>();
    }
}